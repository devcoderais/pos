class ProductsController < ApplicationController

  def index
    if params[:search_query].blank?
      @products = Product.includes(:tags, :taggings).all.page params[:page]
    else
      q_array_with_percetage_signs = params[:search_query].split(",").map {|val| "%#{val}%" }
      @products = Product.where(id: Tagging.where(tag_id: Tag.where("name ILIKE ANY ( array[?] )", q_array_with_percetage_signs).pluck(:id)).pluck(:product_id).uniq).page params[:page]
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save!
      redirect_to @product, notice: "Product was successfully created."
    else
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      redirect_to edit_product_path(@product), notice: "Product was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, alert: 'Product was successfully destroyed.' }
    end
  end

  private

  def product_params
    params.require(:product).permit(:name, :price, :search_query, tag_ids: [])
  end 
end
