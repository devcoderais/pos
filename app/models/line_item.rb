class LineItem < ApplicationRecord
  # Use optional: `true` to add line_item to invoice before invoice object actually exists
  belongs_to :invoice, optional: true
  belongs_to :product
  # accepts_nested_attributes_for :product
end
