class Invoice < ApplicationRecord
  has_many :line_items, inverse_of: :invoice,  dependent: :destroy
  has_many :products, through: :line_items
  belongs_to :user

  accepts_nested_attributes_for :line_items, allow_destroy: true
end
