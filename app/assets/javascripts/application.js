// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets
//= require activestorage
//= require turbolinks
//= require_tree .


$(document).on('ready turbolinks:load', onPageReadyPlugins);

function onPageReadyPlugins(){

  $('form').on('click', '.remove_record', function(event){
    $(this).prev("input[type=hidden]").val('1');
    $(this).closest('tr').hide();
    return event.preventDefault();
  });


  $('form').on('click', '.add_fields', function(event){
    var regexp, time;
    time = new Date().getTime();
    regexp = new RegExp($(this).data('id'), 'g');
    $('.fields').append($(this).data('fields').replace(regexp, time));
    // Prevent bubbling up of ancestors elements in the DOM tree
    event.stopImmediatePropagation()
    return event.preventDefault();
  });


  $("form").on('keyup input change', ".product-listing , .product-quantity", function(event){
    var productPrices = $(this).data('prices');
    var totalAmount = 0;
    var amount = 0;

    // Show price as per dropdown selection 
    if (!$(this).hasClass("product-quantity")){
      var currentRow = $(this).closest("tr");
      var dropdownPrice = productPrices[parseInt($(this).val())];
      currentRow.find("td:eq(3)").html("<td>"+dropdownPrice+"</td>");
    }

    var quantities = row_quantities();
    var prices = row_prices(productPrices);
    var amounts = row_amounts(quantities, prices);
    if (amounts.length != 0){
      $.each(amounts,function(){amount+=(this) || 0; });
      console.log(amount);
    }
    $('form').find("#amount-without-tax").attr('value', ""+amount)
    var totalAmount = amount_after_tax(amount);
 });

}

function row_quantities(){
  var quantity_arr = [];
  $('.dynamic-add tbody tr td:nth-child(2)').each( function(index, value){

    var quantity = value.children[0].lastElementChild.id
    quantity_arr.push([index, $("#"+quantity).val()]);
  });
  return quantity_arr
}

function row_prices(productPrices){
  var price_arr=[];
  $('.dynamic-add tbody tr td:nth-child(3)').each( function(index, value){
    var pro = value.children[0].id
    var product_id =  $("#"+pro).val();
    if ((productPrices != undefined) && (product_id != undefined)) {
      var price = productPrices[parseInt(product_id)]
      price_arr.push([index, price]);
    }
  });
  return price_arr
}

function row_amounts(quantity_arr, price_arr){
  var amount_arr = [];
  $.each(quantity_arr, function(q_index, q_value){
    if ((quantity_arr.length != 0) && !isNaN(parseInt(quantity_arr[q_index][1])) && (price_arr.length != 0) && !isNaN(parseInt(price_arr[q_index][1]))){
      var amount = parseInt(quantity_arr[q_index][1]) * parseInt(price_arr[q_index][1]);
      amount_arr.push(amount);
    }
  });
  return amount_arr
}

function amount_after_tax(amount){
  var taxPercentage = 15;
  var totalAmount = (parseInt(amount) + (amount*taxPercentage/100)).toFixed(2);
  $('form').find("#amount-with-tax").attr('value', ""+totalAmount)
  return totalAmount
}

