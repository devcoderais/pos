# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!(email: "test226@mailinator.com", password: "Password123")
User.create!(email: "test227@mailinator.com", password: "Password123")

Product.create(name: "Coffee", price: 100.00)
Product.create(name: "Tea", price: 75.00)
Product.create(name: "Chicken Sandwich", price: 150.00)
Product.create(name: "Chocolate Cake", price: 125.00)
Product.create(name: "Veg Sandwich", price: 125.00)

Tag.create(name: "Coffee")
Tag.create(name: "Beverage")
Tag.create(name: "Tea")
Tag.create(name: "Sandwich")
Tag.create(name: "Food")
Tag.create(name: "Bread")
Tag.create(name: "Non-Veg")
Tag.create(name: "Chicken")
Tag.create(name: "Chocolate")
Tag.create(name: "Cake")
Tag.create(name: "Dessert")
Tag.create(name: "Veg")