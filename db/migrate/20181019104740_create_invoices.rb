class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.string :title
      t.float :amount
      t.float :tax_percentage
      t.float :total_amount
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
